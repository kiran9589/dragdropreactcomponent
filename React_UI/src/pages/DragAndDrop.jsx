import React from 'react';
import { Card, Row, Col } from 'antd';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import SourceBox from './dndComponent/SourceBox';
import TargetBox from './dndComponent/TargetBox';
import Components from './dndComponent/Components';
import Container from './dndComponent/Container'
import formImage from '../assets/form.png';

const hideSourceOnDrag = true;
export default () => (
  <div style={{ background: '#fff', padding: 20 }}>
    <DndProvider backend={HTML5Backend}>
      <Row gutter={24}>
        <Col xl={6} style={{ padding: '0px' }}>
          <Card title="Toolbox" bordered={true} bodyStyle={{ padding: '0px' }}>
            {Components.map(item => (
              <SourceBox
                name={item.name}
                identifier={item.key}
                icon={item.icon}
                component={item.component}
              />
            ))}
          </Card>
        </Col>
        <Col xl={18} lg={24} md={24} sm={24} xs={24} style={{ padding: '0px' }}>
          <Card title="Dropable Area" bordered={true} bodyStyle={{ padding: '0 8px 0px 20px' }}>
            <Row gutter={24}>
              <Col xl={24} lg={24} md={24} sm={24} xs={24} style={{ padding: '0px', backgroundImage: `url(${formImage})` }}>
                {/* <TargetBox /> */}
                {/* <img src={require('../assets/form.png')} /> */}
                <Container hideSourceOnDrag={hideSourceOnDrag} />
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </DndProvider>
  </div>
);

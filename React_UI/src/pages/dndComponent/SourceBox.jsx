import React, { useState, useCallback, useMemo } from 'react';
import { useDrag } from 'react-dnd';
import { Card, Button } from 'antd';
const style = {
  border: '1px dashed gray',
  padding: '0.5rem',
  margin: '0.5rem',
};

const SourceBox = ({ name, identifier, icon }) => {
  const [{ isDragging }, drag] = useDrag({
    item: { name, identifier, type: identifier },
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();
      if (item && dropResult) {
        //  alert(`You dropped ${item.name} into ${dropResult.name}!`)
      }
    },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });

  return (
    <div ref={drag} style={{ ...style }}>
      <Button type="link" key={name}>
        {' '}
        {name}
      </Button>
    </div>
  );
};

export default SourceBox;

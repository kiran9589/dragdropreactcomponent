import React, { useState } from 'react';
import { useDrop } from 'react-dnd';
import {
  Card,
  Button,
  Form,
  Input,
  InputNumber,
  Select,
  Tag,
  Checkbox,
  Radio,
  DatePicker,
} from 'antd';
import update from 'immutability-helper';
import uuid from 'react-uuid';
import Box from './Box';

const styles = {
  height: '500px',
  width: '100%',
  marginRight: '1.5rem',
  marginBottom: '1.5rem',
  textAlign: 'center',
  fontSize: '1rem',
  lineHeight: 'normal',
  padding: '2rem',
  position: 'relative',
};

const componentMap = {
  // a: { top: 20, left: 80, title: 'Drag me around' },
  // b: { top: 180, left: 20, title: 'Drag me too' },
};

const Container = ({ hideSourceOnDrag, onDrop, lastDroppedColor }) => {
  const [boxes, setBoxes] = useState(componentMap);
  const [{ isOver, draggingComponent, canDrop, delta }, drop] = useDrop({
    accept: [
      'TextInput',
      'NumberInput',
      'TextArea',
      'Dropdown',
      'Tags',
      'Checkboxes',
      'RadioButtons',
      'Date',
      'box',
    ],

    drop(item, monitor) {
      const delta = monitor.getDifferenceFromInitialOffset();
      const left = Math.round((item.left || 0) + delta.x);
      const top = Math.round((item.top || 0) + delta.y);

      if (!item.id) {
        // eslint-disable-next-line no-param-reassign
        const itemId = uuid();
        // eslint-disable-next-line no-param-reassign
        var compo;
        switch (item.type) {
          case 'TextInput':
            compo = <Input placeholder="Enter something" />;
            break;
          case 'NumberInput':
            compo = <InputNumber min={1} max={10} defaultValue={3} />;
            break;
          case 'TextArea':
            compo = (
              <TextArea
                rows={4}
                placeholder="Controlled autosize"
                autoSize={{ minRows: 3, maxRows: 5 }}
              />
            );
            break;
          case 'Dropdown':
            compo = (
              <Select
                placeholder="Select a person"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            );
            break;
          case 'Tags':
            compo = (
              <div>
                <Tag closable>Tag 1</Tag>
                <Tag closable>Tag 2</Tag>
                <Tag closable>Tag 3</Tag>
              </div>
            );
            break;
          case 'Checkboxes':
            compo = <Checkbox>Checkbox 1</Checkbox>;
            break;
          case 'RadioButtons':
            compo = (
              <Radio.Group name="radiogroup" defaultValue={1}>
                <Radio value={1}>A</Radio>
                <Radio value={2}>B</Radio>
                <Radio value={3}>C</Radio>
                <Radio value={4}>D</Radio>
              </Radio.Group>
            );
            break;
          case 'Date':
            compo = <DatePicker />;
            break;
          default:
            break;
        }

        item.id = itemId;
        componentMap[itemId] = {
          top: top,
          left: left,
          title: item.name,
          compo: compo,
          identifier: item.identifier,
          type: item.type,
        };
      } else {
        componentMap[item.id].left = left;
        componentMap[item.id].top = top;
      }
      // eslint-disable-next-line no-shadow

      moveBox(item.id, left, top, componentMap);
      return undefined;
    },
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
      draggingComponent: monitor.getItemType(),
      delta: monitor.getDifferenceFromInitialOffset(),
    }),
  });
  const moveBox = (id, left, top, items) => {
    // debugger;
    setBoxes(
      update(items, {
        [id]: {
          $merge: { left, top },
        },
      }),
    );
  };
  return (
    <div ref={drop} style={styles}>
      {Object.keys(boxes).map(key => {
        const { left, top, title, compo, identifier } = boxes[key];
        return (
          <Box
            key={key}
            id={key}
            left={left}
            top={top}
            hideSourceOnDrag={hideSourceOnDrag}
            identifier={identifier}
          >
            {compo}
          </Box>
        );
      })}
    </div>
  );
};
export default Container;

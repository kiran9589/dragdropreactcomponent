import React, { useState, useCallback } from 'react';
import { useDrop } from 'react-dnd';
import {
  Card,
  Button,
  Form,
  Input,
  InputNumber,
  Select,
  Tag,
  Checkbox,
  Radio,
  DatePicker,
} from 'antd';
const { Option } = Select;

const { TextArea } = Input;
const style = {
  height: '100%',
  width: '100%',
  marginRight: '1.5rem',
  marginBottom: '1.5rem',
  color: 'white',
  padding: '1rem',
  textAlign: 'center',
  fontSize: '1rem',
  lineHeight: 'normal',
  padding: '2rem',
  textAlign: 'center',
};

const mt10 = {
  margin: '5px 0',
};

var componentList = [];

const TargetBox = ({ onDrop, lastDroppedColor }) => {

  const [{ isOver, draggingComponent, canDrop, delta }, drop] = useDrop({
    accept: [
      'TextInput',
      'NumberInput',
      'TextArea',
      'Dropdown',
      'Tags',
      'Checkboxes',
      'RadioButtons',
      'Date',
    ],
    drop(item) {
      // onDrop(item.type)
      switch (item.type) {
        case 'TextInput':
          componentList.push(<Input placeholder="Enter something" />);
          break;
        case 'NumberInput':
          componentList.push(<InputNumber min={1} max={10} defaultValue={3} />);
          break;
        case 'TextArea':
          componentList.push(
            <TextArea
              rows={4}
              placeholder="Controlled autosize"
              autoSize={{ minRows: 3, maxRows: 5 }}
            />,
          );
          break;
        case 'Dropdown':
          componentList.push(
            <Select
              placeholder="Select a person"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>,
          );
          break;
        case 'Tags':
          componentList.push(
            <div>
              <Tag closable>Tag 1</Tag>
              <Tag closable>Tag 2</Tag>
              <Tag closable>Tag 3</Tag>
            </div>,
          );
          break;
        case 'Checkboxes':
          componentList.push(<Checkbox>Checkbox 1</Checkbox>);
          break;
        case 'RadioButtons':
          componentList.push(
            <Radio.Group name="radiogroup" defaultValue={1}>
              <Radio value={1}>A</Radio>
              <Radio value={2}>B</Radio>
              <Radio value={3}>C</Radio>
              <Radio value={4}>D</Radio>
            </Radio.Group>,
          );
          break;
        case 'Date':
          componentList.push(<DatePicker />);
          break;
        default:
          break;
      }

      const left = Math.round(0 + delta.x);
      const top = Math.round(0 + delta.y);
      moveBox(item.type, left, top);

      return componentList;
    },
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
      draggingComponent: monitor.getItemType(),
      delta: monitor.getDifferenceFromInitialOffset(),
    }),
  });

  // const opacity = isOver ? 1 : 0.7

  const moveBox = (id, left, top) => {
    debugger;
  };

  const handleSubmit = e => {
    e.preventDefault();
  };
  return (
    <div ref={drop} className="example-input" style={{ ...style }}>
      <Form onSubmit={handleSubmit} className="login-form">
        {componentList.map(item => (
          <div style={{ ...mt10 }}>
            <Form.Item>{item}</Form.Item>
          </div>
        ))}
      </Form>
    </div>
  );
};
const StatefulTargetBox = props => {
  return <TargetBox {...props} />;
};
export default StatefulTargetBox;

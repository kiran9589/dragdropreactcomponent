// export default {
//     YELLOW: 'red',
//     BLUE: 'green',
//   }

const items = [
  {
    key: 'TextInput',
    name: 'Text Input',
    icon: 'fa fa-font',
  },
  {
    key: 'NumberInput',
    name: 'Number Input',
    icon: 'fa fa-plus',
  },
  {
    key: 'TextArea',
    name: 'Multi-line Input',
    icon: 'fa fa-text-height',
  },
  {
    key: 'Dropdown',
    name: 'Dropdown',
    icon: 'fa fa-caret-square-o-down',
  },
  {
    key: 'Tags',
    name: 'Tags',
    icon: 'fa fa-tags',
  },
  {
    key: 'Checkboxes',
    name: 'Checkboxes',
    icon: 'fa fa-check-square-o',
  },
  {
    key: 'RadioButtons',
    name: 'Multiple Choice',
    icon: 'fa fa-dot-circle-o',
  },
  {
    key: 'Date',
    name: 'Date',
    icon: 'fa fa-calendar',
  },
];

export default items;

import React from 'react';
import { useDrag } from 'react-dnd';

const style = {
  position: 'absolute',
  border: '1px dashed gray',
  backgroundColor: 'white',
  // padding: '0.5rem 1rem',
  cursor: 'move',
};
const Box = ({ id, left, top, hideSourceOnDrag, identifier, children }) => {
  const [{ isDragging }, drag] = useDrag({
    item: { id, left, top, type: identifier },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });
  if (isDragging && hideSourceOnDrag) {
    return <div ref={drag} />;
  }
  return (
    // ...style,
    <div ref={drag} style={{ ...style, left, top }}>
      {children}
    </div>
  );
};
export default Box;
